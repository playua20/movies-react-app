import cn from 'classnames';
import PT from 'prop-types';

import './Loader.scss';

export const Loader = ({ className }) => (
    <div className={cn('loader', className)}>
        <div className="loader-dot"></div>
        <div className="loader-dot"></div>
        <div className="loader-dot"></div>
        <div className="loader-dot"></div>
        <div className="loader-dot"></div>
        <div className="loader-dot"></div>
    </div>
);

Loader.propTypes = {
    // Additional loader's class name
    className: PT.string
};
