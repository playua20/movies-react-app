import { Route, Routes, Navigate } from 'react-router-dom';

import { LayoutContainer } from '../../containers/LayoutContainers/LayoutContainer';
import { HomePage } from '../../pages/HomePage/HomePage';
import { MovieDetailsPage } from '../../pages/MovieDetailsPage/MovieDetailsPage';
import { AuthPage } from '../../pages/AuthPage/AuthPage';
import { useDocumentTitle } from '../../hooks/useDocumentTitle';
import { useAutoLogin } from '../../hooks/useAutoLogin';

import './App.scss';

export const App = () => {
    useDocumentTitle();
    useAutoLogin();

    return (
        <div className="app">
            <LayoutContainer>
                {({ movies }) => (
                    <Routes>
                        <Route path="/profile" element={<h3>Profile</h3>}></Route>

                        <Route
                            path="/movie/:movieId"
                            element={<MovieDetailsPage movies={movies} />}
                        ></Route>

                        <Route path="/auth" element={<AuthPage />}></Route>

                        <Route path="/" exact element={<HomePage movies={movies} />}></Route>

                        <Route path="*" element={<Navigate to="/" />} />
                    </Routes>
                )}
            </LayoutContainer>
        </div>
    );
};
