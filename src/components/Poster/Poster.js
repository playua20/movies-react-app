import { useState } from 'react';
import cn from 'classnames';
import PT from 'prop-types';

import './Poster.scss';

export const Poster = ({ className, frontImage, backImage, alt }) => {
    const [isHovered, setIsHovered] = useState(false);
    return (
        <div
            className={cn('poster', className)}
            onMouseOver={() => setIsHovered(true)}
            onMouseOut={() => setIsHovered(false)}
        >
            <img
                src={frontImage}
                alt={alt}
                className={cn('poster__image poster__image--front', {
                    'poster__image--hidden': backImage && isHovered
                })}
            />
            {!!backImage && (
                <img src={backImage} alt={alt} className="poster__image poster__image--back" />
            )}
        </div>
    );
};

Poster.propTypes = {
    // Additional poster-s class name
    className: PT.string,
    // Front image path
    frontImage: PT.string.isRequired,
    // Back image path
    backImage: PT.string,
    //Alt attribute for both image
    alt: PT.string //.isRequired
};
