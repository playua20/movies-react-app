import { Component } from 'react/cjs/react.production.min';

export class ErrorBoundary extends Component {
    state = {
        error: null
    };

    static getDerivedStateFromError(error) {
        return { error };
    }

    render() {
        const { children } = this.props;
        const { error } = this.state;

        if (!error) return children;

        // TODO можно добавить стили в случае error
        return <div>{error.message}</div>;
    }
}
