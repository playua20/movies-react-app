import PT from 'prop-types';

import { Button } from '../Button/Button';
import { Input } from '../Input/Input';
import { Navigation } from '../Navigation/Navigation';
import './Layout.scss';

export const Layout = ({
    search,
    movies,
    isSearching,
    onChangeSearch,
    onSearchMovies,
    children
}) => (
    <div className="layout">
        <header className="layout__header">
            <div className="centerer  layout__header-wrapper">
                <div className="layout__search-wrapper">
                    <Input
                        name="search"
                        placeholder="Search"
                        disabled={isSearching}
                        value={search}
                        onChange={onChangeSearch}
                    />
                    <Button disabled={isSearching} onClick={onSearchMovies}>
                        {isSearching ? 'Searching' : 'Search'}
                    </Button>
                </div>

                <Navigation />
            </div>
        </header>

        <main className="layout__main">
            <div className="centerer">{children({ movies })}</div>
        </main>

        <footer className="layout__footer">
            <div className="centerer">
                <a href="https://github.com/playua20">
                    <i class="fab fa-github"></i>
                </a>
                <strong className="layout__copyright">
                    All Rights Reserved, {new Date().getFullYear()}
                </strong>
                <a href="https://bitbucket.org/playua20/">
                    <i class="fab fa-bitbucket"></i>
                </a>
            </div>
        </footer>
    </div>
);

Layout.propTypes = {
    // Value for the serch input
    search: PT.string.isRequired,
    // List of fetched movies
    movies: PT.array.isRequired,
    // Indicate whether movies are searching
    isSearching: PT.bool.isRequired,
    // Render function for component presentation
    children: PT.func.isRequired,
    // Callback for changing value of the search input
    onChangeSearch: PT.func.isRequired,
    // Callback for searching movies
    onSearchMovies: PT.func.isRequired
};
