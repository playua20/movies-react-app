import PT from 'prop-types';
import cn from 'classnames';

import './Button.scss';

const DEFAULT_TYPE = 'button';
const DEFAULT_VARIANT = 'primary';
// const SECONDARY_VARIANT = 'secondary';
// const TERTIARY_VARIANT = 'tertiary';

export const Button = ({
    type = DEFAULT_TYPE,
    variant = DEFAULT_VARIANT,
    className,
    children,
    ...other
}) => (
    <button {...other} type={type} className={cn('button', `button--${variant}`, className)}>
        {children}
    </button>
);

// https://react-movies-app-1d1c7-default-rtdb.firebaseio.com/

Button.propTypes = {
    // Type of button
    type: PT.oneOf(['button', 'submit', 'reset']),
    // Variant of button's styling
    variant: PT.oneOf(['primary', 'secondary', 'tertiary']),
    // Additional button's className
    className: PT.string,
    // Button
    children: PT.node.isRequired
};
