import { useState } from 'react';

const DEFAULT_METHOD = 'POST';

const noop = () => null; // бутафорская функция, служит заглушкой для onSuccess и onError, если в них не переданы данные.

// url: initialUrl, method: initialMethod = DEFAULT_METHOD, headers: initialHeaders, body: initialBody,

export const useMutation = (options = {}) => {
    const { onSuccess = noop, onError = noop, ...initialOption } = options;
    const [data, setData] = useState();
    const [error, setError] = useState();
    const [isLoading, setIsLoading] = useState(false);

    const handleMutate = async (body, mutationOptions = {}) => {
        const mergedOptions = { ...initialOption, ...mutationOptions };

        const { url, method = DEFAULT_METHOD, headers, body: initialBody } = mergedOptions;

        setIsLoading(true);

        try {
            const response = await fetch(url, { method, headers, body: body || initialBody });
            const mutationData = await response.json();

            setData(mutationData);
            onSuccess(mutationData);
        } catch (e) {
            setError(e);
            onError(e);
        } finally {
            setIsLoading(false);
        }
    };

    return { mutate: handleMutate, data, error, isLoading };
};
