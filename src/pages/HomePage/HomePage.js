import PT from 'prop-types';
import { Link } from 'react-router-dom';

import noImage from '../../resources/images/no-image.png';
import './HomePage.scss';

const { REACT_APP_STORAGE_APP: storageApp } = process.env;

export const HomePage = ({ movies }) => (
    <div className="home-page">
        {movies.length ? (
            <ul className="home-page__list">
                {movies.map(({ id, original_title, poster_path }) => {
                    const posterUrl = poster_path ? `${storageApp}w500${poster_path}` : noImage;

                    return (
                        <li
                            key={id}
                            className="home-page__list-item"
                            style={{ backgroundImage: `url(${posterUrl})` }}
                        >
                            <Link
                                to={`/movie/${id}`}
                                className="home-page__link"
                                title={original_title}
                            >
                                <h2 className="home-page__title">{original_title}</h2>
                            </Link>
                        </li>
                    );
                })}
            </ul>
        ) : (
            <p class="home-page__empty-list">
                Воспользуйтесь поиском что бы найти желаемый сериал/фильм на TMDB
            </p>
        )}
    </div>
);

HomePage.propTypes = {
    // List of the movies fetched from the Movies API
    movies: PT.arrayOf(
        PT.shape({
            id: PT.number.isRequired,
            poster_path: PT.string,
            original_title: PT.string.isRequired
        })
    ).isRequired
};
