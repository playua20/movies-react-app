import { BrowserRouter } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider as ReduxProvider } from 'react-redux';

// import Swiper, { Navigation, Pagination, Scrollbar } from 'swiper';
// import 'swiper/css';
// import 'swiper/css/navigation';
// import 'swiper/css/pagination';
// import 'swiper/css/scrollbar';
// new Swiper('.my-swiper', {
//     // pass modules here
//     modules: [Navigation, Pagination, Scrollbar]
//     // ...
// });

import { App } from './components/App/App';
import { ErrorBoundary } from './components/ErrorBoundary/ErrorBoundary';
import { rootReducer } from './store/root';
import './styles/index.scss';

const store = createStore(
    rootReducer /* preloadedState, */,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export const Root = () => (
    <ReduxProvider store={store}>
        <BrowserRouter>
            <ErrorBoundary>
                <App />
            </ErrorBoundary>
        </BrowserRouter>
    </ReduxProvider>
);
